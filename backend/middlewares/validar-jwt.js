const { response, request } = require('express');
const jwt = require('jsonwebtoken');
const Usuario = require('../models/user');

const validarJWT = async(req = request, res = response, next) => {

    const token =  req.header('x-token');
    console.log(token);


    if(!token){
        return res.status(401).json({
            msg: 'No hay token en la peticion'
        })
    }

    try {
        const {uid} = jwt.verify(token, process.env.SECRETORPUBLICKEY);

        //leer usuario
        const usuarioAutenticado = await Usuario.findOne({_id:uid});

        if(!usuarioAutenticado){
            return res.status(401).json({
                msg: 'Token no valido - usuario no existe en la base de datos'
            })
        }

        //Verificar si el estado es true
        if(!usuarioAutenticado.estado){
            return res.status(401).json({
                msg: 'Token no valido - usuario estado false'
            })
        }
        
        req.usuarioAutenticado = usuarioAutenticado;
        next();
    } catch (error) {
        console.log(error);
        res.status(401).json({
            msg: 'Token no valido'
        })
    }

    next();

}


module.exports = {
    validarJWT
}