const Usuario = require("../models/user");



const validarEmail = async(correo)=> {
    const existeEmail = await Usuario.findOne({correo});
    if(existeEmail){
        throw new Error(`El rol ${correo} ya existe`);
    }
}

const existeUsuarioPorId = async(id)=> {
    const existeUsuario = await Usuario.findById(id);
    if(!existeUsuario){
        throw new Error('El id no existe')
    }
}

module.exports= {
    validarEmail,
    existeUsuarioPorId
}