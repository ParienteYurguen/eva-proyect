const express =  require('express');
const cors = require('cors');
const { dbConnection } = require('../database/config');
class Server {


    constructor(){
        this.app = express();
        this.port = process.env.PORT;
        this.usuariosPath = '/api/usuarios';
        this.authPath = '/api/auth';
        this.palabrasPath = '/api/palabras';
        this.archivoPath = "/api/archivo";
        this.scormPath = "/api/scorm"

        //Conectar a base de datos
        this.conectarDB();
        //Middelwares
        this.middelware();
        //Rutas de mi aplicacion
        this.routes();
    }

    async conectarDB(){
        await dbConnection();
    }

    middelware(){
        //CORS
        this.app.use(cors());

        //Lectura y paseo del body
        this.app.use(express.json());

        //Directorio public
        this.app.use(express.static('public'));
    }

    routes(){         
        
        this.app.use(this.usuariosPath, require('../routes/user.routes'));
        this.app.use(this.authPath, require('../routes/auth.routes'));
        this.app.use(this.palabrasPath,require('../routes/palabras.routes'));
        this.app.use(this.archivoPath,require("../routes/archivo.routes"));
        this.app.use(this.scormPath,require("../routes/scorm.routes"))
    }

    listen(){
        this.app.listen(this.port, ()=> {
            console.log('Servidor corriendo en el puerto', this.port);
        });
    }
}

module.exports = Server;