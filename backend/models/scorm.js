
const { Schema, model } = require('mongoose');


const scormSchema = Schema({
    idUser:{
        type:String
    },
    palabrasBorrar: {
        type: [String]
    },
    palabrasObtenidas: {
        type: [String]
    },
    nombre:{
        type:String
    },
    tipo:{
        type:String
    }
});


module.exports = model('Scorm', scormSchema); 