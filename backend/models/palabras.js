
const { Schema, model } = require('mongoose');


const palabrasSchema = Schema({
    palabras: {
        type: [String],
    }
});


module.exports = model('Palabra', palabrasSchema); 