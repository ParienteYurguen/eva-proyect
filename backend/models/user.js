
const { Schema, model } = require('mongoose');


const usuarioSchema = Schema({
    nombre: {
        type: String,
        required: [true, 'El nombre es obligatorio']
    },
    correo: {
        type: String,
        required: [true, 'El correo es obligatorio'],
        unique: true
    },
    password: {
        type: String,
        required: [true, 'La contrasena es obligatoria']
    }

});

usuarioSchema.methods.toJSON = function(){
    const {__v, password,_id, ...user} = this.toObject();
    user.uid = _id;
    return user;
}

module.exports = model('Usuario', usuarioSchema); 