const {response} = require('express')
const Palabras = require('../models/palabras');

const palabrasGet = async(req, res=response) => {
    const palabras = await Palabras.find();
    res.json({
        palabras
    })
}

const palabrasPost =  async(req, res = response) => {

    const {palabras} = req.body;
    const pal = new Palabras({palabras});

    await pal.save();
    res.json({
    ok:true,
    pal
    })
}

module.exports = {
    palabrasGet,
    palabrasPost
}