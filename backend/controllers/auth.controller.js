const { response, json } = require("express");
const Usuario = require('../models/user')
const bcryptjs = require('bcryptjs');
const { generarJWT } = require("../helpers/generarJWT");
const login = async(req, res = response) => {

    const {correo,password} =  req.body;

    try {

        //Verificar si el correo existe
        const usuario = await Usuario.findOne({correo});
        if(!usuario){
            return res.status(400).json({
                msg:'Correo incorrecto'
            })
        }
        
        //Verificar contrasena
        const validPassword = bcryptjs.compareSync(password, usuario.password);
        if(!validPassword){
            return res.status(400).json({
                ok:false,
                msg:'Password incorrecto'
            })
        }
        //Generar JWT
        const token = await generarJWT(usuario._id);

        res.json({
            usuario,
            ok:true,
            token
        })
    } catch (error) {
        console.log(error)
        return res.status(500).json({
            msg:'Hable con el administrador'
        })
    }


}
module.exports = {
    login
}