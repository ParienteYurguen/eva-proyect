const { response } = require("express");
const path = require("path");
const fs = require("fs");
const JSZip = require('jszip');
const {saveAs} = require('file-saver') ;

const generarArchivo = async (req, res = response) => {
    const direccion = path.resolve(__dirname);
    const arreglo = direccion.split("C:\\");
    const original = arreglo[1];
    fs.appendFile(`/${original}/scorm/document.js`,"var scorm = pipwerks.SCORM;"+"let respuestas = ['','',''];"+
"const array = ['tal','estas','bien'];"+
"let i = 0;"+
        `function init(){
            scorm.version = "1.2";
            Mensaje("Iniciando el Curso.");
            cargarTexto()
            llenarPalabras();
            var callSucceeded = scorm.init();
            Mensaje("Curso iniciado correctamente? " + callSucceeded);
        }
        function ObtenerScore(){
            var score = scorm.get("cmi.core.score.max");
            console.log(score);
            return score;
        }
        function AumentarScore(){
            scorm.set("cmi.core.score.max","25");
        }
        function CompletarCurso(){
            Mensaje("Marcando curso como Completado.");
            var callSucceeded = scorm.set("cmi.core.lesson_status", "completed");
            Mensaje("Curso Completado? " + callSucceeded);
        }`+
    
       " function eliminarRespuesta(indice){respuestas.splice(indice,1,'');document.getElementById(`${indice}`).innerHTML = '';document.getElementById(`pal${indice}`).hidden = false;}"+
    
       "function cargarRespuesta(pal = ''){let bandera = false;let i = 0;while(i< respuestas.length && bandera===false){let docu = document.getElementById(`${i}`);let boton = document.getElementById(`pal${i}`);if(respuestas[i]===''){respuestas.splice(i,1,pal);bandera=true;docu.innerHTML = pal;boton.hidden = true;}i++;}}"+
       " function llenarPalabras(){for(let i=0;i<array.length;i++){document.getElementById('contenedor').insertAdjacentHTML('beforeend',`<button class='btn btn-danger mt-2 me-1 text-center  col-12' id='pal${i}' onclick='cargarRespuesta('${array[i]}')'>${array[i]}</button>`)}}"+
    
        `function ObtenerNombre(){
            var nombreUser = scorm.get("cmi.core.student_name");
            var scoreUser = scorm.get("cmi.core.score.max");
            document.getElementById('Nombre').innerHTML = '<b>' + nombreUser+"_"+ scoreUser  + '</b>';
        }
    `+
        `function end(){
            Mensaje("Conexion terminada.");
            var callSucceeded = scorm.quit();
            Mensaje("Termino correctamente? " +callSucceeded);
        }
    
        function Mensaje(msg){
            alert(msg);
        }`+
        
        "function cargarTexto(){let palabra = 'Hola que  como , yo muy'; let array = [];let j = 0;palabra = palabra.replace(/,/g, '');array = palabra.split(' ');for(let palabra of array){if(palabra === ''){document.getElementById('contenedorPalabras').insertAdjacentHTML('beforeend',`<span class='me-1 alert alert-danger' onclick='eliminarRespuesta('${j}')' id='${j}' >${respuestas[j]}</span>`);j++;}else{document.getElementById('contenedorPalabras').insertAdjacentHTML('beforeend',`<span class='me-1' >${palabra}</span>`);}}}"+
    
       ` window.onload = function (){
            init();
        }
    
        window.onunload = function (){
            end();
        }`,(error)=>{
        if(error){
            return res.json({
                ok:false
            })
        }
        console.log("El archivo ha sido creado");
        comprimirZip();
        return res.json({
            ok:true
        })
    })
}

function comprimirZip(){
  var zip = new JSZip();

    zip.file("hola.txt","Hola mundo",{binary:false});
    zip.generateAsync({type:"nodebuffer"}).then((content)=> {
        saveAs(content,'hola.zip')
    });



}
module.exports = {
    generarArchivo
}