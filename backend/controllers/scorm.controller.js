const {response} = require('express')
const Scorm = require('../models/scorm');

const scormGet = async(req, res=response) => {
    const scorms = await Scorm.find();
    return res.json({
        scorms
    })
}

const scormGetUser = async(req,res=response) => {
    const {id} = req.params;
    const scorms = await Scorm.find({idUser:id});
    return res.json({
        scorms
    })
}

const scormGetId = async(req,res=response) => {
    const {id} = req.params;
    const scorm = await Scorm.findOne({_id:id});
    return res.json({
        scorm
    })
}

const scormPut =  async(req, res = response) => {
    const {id} = req.params;
    const {_id,idUser,nombre, ...resto} = req.body;
    const scorm = await Scorm.findByIdAndUpdate(id, resto);
    res.json({
        scorm
    })
}

const scormPost =  async(req, res = response) => {

    const {idUser,palabrasBorrar,palabrasObtenidas,nombre,tipo} = req.body;
    const scorm = new Scorm({idUser,palabrasBorrar,palabrasObtenidas,nombre,tipo});

    //Guardar en DB
    await scorm.save();
    res.json({
    ok:true,
    scorm
    })
}

const scormDelete = async (req, res) => {
    const {id} = req.params;
    //Borramos fisicamente
    // const usuario = Usuario.findByIdAndDelete(id);

    const scorm =  await Scorm.findByIdAndDelete(id)
    res.json({
    scorm
    })
}

module.exports = {
   scormGet,
   scormDelete,
   scormPost,
   scormPut,
   scormGetUser,
   scormGetId
}