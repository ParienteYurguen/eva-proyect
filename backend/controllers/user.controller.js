const {response} = require('express')
const bcryptjs = require('bcryptjs');
const { generarJWT } = require("../helpers/generarJWT");
const Usuario = require('../models/user');

const usuariosGet = async(req, res=response) => {
    const usuarios = await Usuario.find();
    res.json({
        usuarios
    })
}

const usuarioPut =  async(req, res = response) => {
    const {id} = req.params;
    const {_id, password, correo,...resto} = req.body;
    if(password){
        const salt = bcryptjs.genSaltSync();
        resto.password = bcryptjs.hashSync(password,salt);
    }
    const usuario = await Usuario.findByIdAndUpdate(id, resto);
    res.json({
        usuario
    })
}

const usuarioPost =  async(req, res = response) => {

    const {nombre,correo,password} = req.body;
    const usuario = new Usuario({nombre,correo,password});

    //Verificar  si el correo existe
    
    //Encriptar la contra
    const salt = bcryptjs.genSaltSync();
    usuario.password = bcryptjs.hashSync(password,salt);
    //Token
    const token = await generarJWT(usuario._id);
    //Guardar en DB
    await usuario.save();
    res.json({
    ok:true,
    token,
    usuario
    })
}

const usuarioDelete = async (req, res) => {
    const {id} = req.params;
    //Borramos fisicamente
    // const usuario = Usuario.findByIdAndDelete(id);

    const usuario =  await Usuario.findByIdAndUpdate(id)
    const usuarioAutenticado = req.usuarioAutenticado;
    res.json({
    usuario,
    usuarioAutenticado
    })
}

module.exports = {
    usuariosGet,
    usuarioPut,
    usuarioDelete,
    usuarioPost
}