const {Router} = require('express');
const { scormGet, scormPost, scormPut, scormDelete, scormGetUser, scormGetId } = require('../controllers/scorm.controller');


const router = Router();

router.get('/', scormGet);
router.post('/',scormPost);
router.put('/:id',scormPut);
router.delete('/:id',scormDelete);
router.get('/user/:id',scormGetUser);
router.get('/:id',scormGetId)





    

module.exports = router;