const {Router} = require('express');
const { check } = require('express-validator');

const { usuariosGet, usuarioPut, usuarioPost, usuarioDelete } = require('../controllers/user.controller');
const {  validarEmail, existeUsuarioPorId } = require('../helpers/db-validators');

const {
    validarCampos,
    validarJWT,
} = require('../middlewares');


const router = Router();

    router.get('/', usuariosGet);

    router.put('/:id',[
        check('id', 'No es un id valido').isMongoId(),
        check('id').custom(existeUsuarioPorId),
        validarCampos
    ], usuarioPut);

    router.post('/',[
        check('correo', 'El correo no es valido').isEmail(),
        check('correo').custom(validarEmail),
        check('password', 'El password debe ser de mas de 6 letras').isLength({min:6}),
        //check('rol','No es un rol permitido').isIn(['ADMIN_ROL','USER_ROL']),
        check('nombre', 'El nombre no es valido').not().isEmpty(),
        validarCampos
    ], usuarioPost);

    router.delete('/:id', [
        validarJWT,
        //esAdminRole,
        check('id','No es un id valido').isMongoId(),
        check('id').custom(existeUsuarioPorId),
        validarCampos
    ] ,usuarioDelete);

module.exports = router;