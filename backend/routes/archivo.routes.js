const {Router} = require('express');
const { generarArchivo } = require('../controllers/archivo.controller');


const router = Router();

router.get("/generador",generarArchivo);

module.exports = router;