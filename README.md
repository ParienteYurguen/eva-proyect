# EVA-Proyect

Evaluacion y evolucion del proyecto para la materia de Entornos Virtuales de Aprendizaje

Rama de version final del proyecto: MAIN

Arquitectura basica:
1. Angular:
 ![imagen-1.png](./imagen-1.png)
2. SCORM
MVC
![imagen-2.png](./imagen-2.png)

Requerimientos:
1. Node.js (instalacion en el siguiente link: https://nodejs.org/en/)
2. Nodemon (instalacion con el comando npm i nodemon)
3. Angular (instalacion con el comando npm install -g @angular/cli)

Instalacion:
1. Clonar repositorio
2. Abrir terminal en la carpeta del proyecto
3. Ingresar a la carpeta "backend"
4. Instalar dependencias con el comando npm install
5. Iniciar backend con el comando nodemon app.js
6. Ingresar a la carpeta "editor"
7. Instalar dependencias con el comando npm install
8. Iniciar frontend con el comando ng serve -o

Credenciales de usuario
correo: yurguen@gmail.com
contrasena: 123465789
