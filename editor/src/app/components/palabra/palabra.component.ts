import { AfterContentInit, Component, Input, OnInit } from '@angular/core';
import { PagesService } from '../../pages/services/pages.service';

@Component({
  selector: 'app-palabra',
  templateUrl: './palabra.component.html',
  styleUrls: ['./palabra.component.css']
})
export class PalabraComponent implements OnInit, AfterContentInit {
  @Input() palabra:string = "";
  @Input() indice:number = -1;
  @Input() jugando:boolean = false;
  @Input() clase:boolean = false;
  @Input() bandera:boolean = false;
  click:boolean = false;

  constructor(private pagesService: PagesService) { }
  ngAfterContentInit(): void {
  }

  ngOnInit(): void {
    this.cambiar2();
  }

  cambiar2(){
    if(this.clase ){
     // this.click = true;
      this.agregar();
    }
  }

  cambiarEstado(){
    this.click = !this.click;
  }
  agregar(){
    this.cambiarEstado();
    if(this.click){
      this.pagesService.agregarBorrar(this.palabra);
    }else{
      this.pagesService.eliminarBorrar(this.palabra);
    }
  }
  // agregar(){
  //   if(this.jugando){
  //     if(this.click===false){
  //       this.pagesService.agregarRespuesta(this.palabra);
  //     }else{
  //       this.pagesService.respuestaAñadida(this.palabra)
  //     }
  //   }else{ 
  //     if(this.click===false){
  //       this.pagesService.agregarPalabra(this.palabra,this.indice,false);
  //     }else{
  //       this.pagesService.borrarPalabra(this.palabra)
  //       this.pagesService.recuperarPalabra(this.indice,this.palabra);
  //   }
  //   }
  // }
  cambiarClase():string{
    if(this.click){
      return 'alert alert-primary'
    }else{
      return 'alert alert-danger'
    }
  }


}
