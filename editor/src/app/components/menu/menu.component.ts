import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-menu',
  templateUrl: './menu.component.html',
  styleUrls: ['./menu.component.css']
})
export class MenuComponent implements OnInit {

  constructor() { }
  nombre:string = ''
  ngOnInit(): void {
    this.nombre = JSON.parse(localStorage.getItem('uid')!).nombre;
  }

}
