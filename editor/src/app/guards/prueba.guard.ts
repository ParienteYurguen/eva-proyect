import { Injectable } from '@angular/core';
import { ActivatedRouteSnapshot, CanActivate, CanLoad, Route, RouterLink, RouterStateSnapshot, UrlSegment, UrlTree } from '@angular/router';
import { Observable } from 'rxjs';
import { PagesService } from '../pages/services/pages.service';

@Injectable({
  providedIn: 'root'
})
export class PruebaGuard implements CanActivate, CanLoad {
  constructor(private pagesService:PagesService){}
  canActivate(
    route: ActivatedRouteSnapshot,
    state: RouterStateSnapshot): Observable<boolean> | Promise<boolean> | boolean {

    return this.verificar();
  }
  canLoad(
    route: Route,
    segments: UrlSegment[]): Observable<boolean> | Promise<boolean> | boolean {
    return this.verificar();
  }

  verificar():boolean{
    const {palabrasBorrar,palabrasObtenidas} = this.pagesService;
    if(palabrasBorrar.length > 0 && palabrasObtenidas.length>0){
      return true;
    } else{
      if(palabrasObtenidas.length === 0){
        alert('No hay datos disponibles para la prueba por favor guardelos');
      }else if(palabrasBorrar.length === 0){
        alert('Por favor seleccione las palabras que se ocultaran al estudiante');
      }
      return false;
    }
    
  }
}
