export interface AuthResponse{
    ok: boolean;
    token?: string;
    usuario:Usuario
}
export interface Usuario{
    nombre:string;
    correo:string;
    _id:string;
}