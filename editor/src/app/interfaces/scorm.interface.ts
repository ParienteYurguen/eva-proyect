export interface Scorm {
    idUser:string;
    palabrasBorrar:string[];
    palabrasObtenidas:string[];
    nombre:string;
    tipo:string;
    _id?:string
}
export interface Respuesta2{
    scorm:Scorm;
}
export interface Respuesta {
    scorms:Scorm[];
} 