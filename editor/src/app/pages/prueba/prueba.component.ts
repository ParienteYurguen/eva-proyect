import { Component, OnDestroy, OnInit } from '@angular/core';
import { PagesService } from '../services/pages.service';
import * as JSZip from 'jszip';
import { ZipService } from '../services/zip.service';
import { saveAs } from 'file-saver';
import { ScormsService } from '../services/scorms.service';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Scorm } from 'src/app/interfaces/scorm.interface';
import { Router } from '@angular/router';
@Component({
  selector: 'app-prueba',
  templateUrl: './prueba.component.html',
  styleUrls: ['./prueba.component.css']
})
export class PruebaComponent implements OnInit, OnDestroy {
  palabras:string[]= [];
  obtenidas:string[]=[];
  respuestas:string[] = [];
  indi:number=-1
  j:number = 0;
  scorm:Scorm = {
    idUser:'',
    palabrasBorrar:[],
    palabrasObtenidas:[],
    nombre:'',
    tipo:'',
    _id:''
  }
  constructor(public pageService:PagesService,private zip:ZipService, private scormService:ScormsService,private fb:FormBuilder,
    private router:Router) { }
  ngOnDestroy(): void {
    this.palabras= [];
    this.obtenidas=[];
    this.respuestas = [];
  }
  aumentarJ(){
    if(this.j<this.palabras.length-1){
      this.j=this.j+1;
    }
    console.log(this.j)
  }
  ngOnInit(): void {
    this.agregarDatos();
    this.scorm = this.pageService.scorm;  
    this.leerLocal();
  }
  
  form:FormGroup = this.fb.group({
    nombre:['',Validators.required]
  });

  agregarRespuesta(palabra:string, indice:number){
    this.pageService.agregarRespuesta(palabra);
    const docu = document.getElementById(indice.toString());
    docu!.hidden = true;
    console.log(this.pageService.respuesta)
  }
  agregarDatos(){
    this.palabras=this.pageService.palabrasBorrar;
    this.obtenidas = this.pageService.palabrasObtenidas;
    this.respuestas = this.pageService.respuesta;
  }
  eliminarRespuesta(palabra:string){
    this.pageService.eliminarRespuesta(palabra);
    this.buscarBoton(palabra);
  }
  buscarBoton(palabra:string){
    let docu = '';
    for(let i=0;i<this.palabras.length;i++){
      docu = document.getElementById(i.toString())!.textContent!;
      if(docu === palabra){
        document.getElementById(i.toString())!.hidden = false;
      }
    }
  }

  guardar(){
    const nombre2 = this.form.get('nombre')?.value;
      this.scorm.nombre = nombre2;
      const {idUser,palabrasBorrar,palabrasObtenidas,tipo,nombre} = this.scorm;
      this.scormService.guardarScorm(idUser,palabrasBorrar,palabrasObtenidas,nombre,tipo)
      .subscribe(res => {alert('Scorm guardado con exito!'); this.router.navigateByUrl('/main/scorms')});
  }
  actualizar(){
    this.scormService.editarScorm(this.scorm._id!,this.scorm)
    .subscribe(res => {alert('Scorm actualizado')
    });
  }
  verificarVacios(palabra:string):string{
    if(palabra===" "){
      return `<small class="alert alert-danger">${palabra}</small>`
    }else{
      return palabra
    }
  }
  devolver(num:number){
    let res=""
      this.indi++
      console.log("numero",num,"indi",this.indi,"return",this.respuestas[this.indi],"res",this.respuestas)
    if(num>this.indi){
      return this.pageService.respuesta[this.indi]
    }else{
      this.indi=-1
      return "error"
    }
  }
  // cargarTexto(){
		
	// 	let j = 0;
	// 	for(let palabra of this.obtenidas){
	// 		if(this.pageService.palabrasBorrar.includes(palabra)){
	// 			document.getElementById("contenedorPalabras")!.insertAdjacentHTML("beforeend",
	// 			`<span class="me-1 alert alert-danger" (click)="eliminarRespuesta(${palabra})">${this.pageService.respuesta[j]}</span>`
	// 			);
	// 			j++;
	// 		}else{
	// 			document.getElementById("contenedorPalabras")!.insertAdjacentHTML("beforeend",
	// 			`<span class="me-1" >${palabra}</span>`
	// 			);
	// 		}
	// 	}

	// }
  crearArray(arreglo:string[]):string{
    let res = '[';
    for(let i=0; i<arreglo.length;i++){
      if(i === arreglo.length-1){
        res += `"${arreglo[i]}"]`;
      }else{
        res += `"${arreglo[i]}",`
      }
    }
    return res;
  }
  leerLocal(){
    const local = localStorage.getItem('simple2');
    if(local){
      this.scorm._id = JSON.parse(local).id;
    }else{
      this.scorm._id = '';
    }
  }
  descargar(){
    this.zip.descargarZip(this.palabras,this.obtenidas,this.respuestas);
  }
  verificarRespuesta(){
   if(this.palabras[0] === this.respuestas[0]){
   return 100
   }else{
     return 50
   }
  }

  
}
