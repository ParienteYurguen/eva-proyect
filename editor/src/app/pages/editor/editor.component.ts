import { Component, OnInit } from '@angular/core';
import { FormArray, FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { PagesService } from '../services/pages.service';
import { ScormsService } from '../services/scorms.service';
import { Scorm } from '../../interfaces/scorm.interface';

interface Simple{
  tipo:string;
  borrar:string[];
  obte:string[];
  id?:string
}
interface Option{
  value:string;
  viewValue:string;
}

@Component({
  selector: 'app-editor',
  templateUrl: './editor.component.html',
  styleUrls: ['./editor.component.css']
})
export class EditorComponent implements OnInit {

  palabrasObtenidas: string[] = [];
  aleatorias: string[] = [];
  palabrasbor:string [] = [];
  pal2:string[] = [];
  usuario =  JSON.parse(localStorage.getItem('uid')!);
  id:string | undefined= '';
  scorm:Scorm = {
    idUser:'',
    palabrasBorrar:[],
    palabrasObtenidas:[],
    nombre:'',
    tipo:'',
    _id:''
  }
  scormSimple:Simple = {
    tipo:'',
    obte: [],
    borrar: [],
    id:''
  }
  constructor(private fb:FormBuilder,private pagesService:PagesService,private ar:ActivatedRoute,private scormService:ScormsService,private router:Router){}
  ngOnInit(): void {
    const id = this.ar.snapshot.params.id;
     
    if(id!=='0'){
      this.leerLocal('simple2');
    }else{
      this.leerLocal('simple1')
    }
    this.pagesService.palabrasBorrar = [];
  }
  // palabra:string = '';
  // leerDB(){
  //   const id = this.ar.snapshot.params.id;
  //   if(id!=="0"){
  //     this.scormService.getPorId(id)
  //     .subscribe(res => {
  //       if(res){
  //         const res2 = res.scorm;
  //         this.scorm = res2;
  //         this.palabrasbor = res2.palabrasBorrar;
  //         this.pal2 = res2.palabrasObtenidas;
  //         const local = localStorage.getItem('obtenidas');
  //         if(res2.tipo === '1'){
  //           if(local){
  //             this.leerLocal()
  //           }else{
  //             this.palabrasObtenidas = res2.palabrasObtenidas;
  //           }
  //                 this. miFormulario = this.fb.group({
  //                   texto:[this.palabrasObtenidas.join(" "),[Validators.required, Validators.minLength(15)]],
  //                   tipo:['1',[Validators.required]]
  //                 });
  //               }else{
  //                 if(local){
  //                   this.leerLocal()
  //                 }else{
  //                   this.aleatorias = res2.palabrasObtenidas;
  //                 }
  //                       this. miFormulario = this.fb.group({
  //                         texto:[this.palabrasObtenidas.join(" "),[Validators.required, Validators.minLength(15)]],
  //                         tipo:['3',[Validators.required]]
  //                       });
  //               }
  //       }
  //     })
  //   }else{
  //     if(localStorage.getItem('obtenidas') && localStorage.getItem('borrar')){
  //       localStorage.removeItem('obtenidas');
  //       localStorage.removeItem('borrar')
  //     }
  //   }
  // }
  // leerLocal(){
  //   const local = localStorage.getItem('obtenidas');
  //   this.palabrasObtenidas = JSON.parse(local!);

  // }

  leerBD(){
    const id = this.ar.snapshot.params.id;
    if(id!=='0'){
      this.scormService.getPorId(id)
      .subscribe(res =>{
        this.id= res.scorm._id;
        this.palabrasObtenidas = res.scorm.palabrasObtenidas;
        this.palabrasbor = res.scorm.palabrasBorrar;
        this. miFormulario = this.fb.group({
          texto:[res.scorm.palabrasObtenidas.join(" "),[Validators.required, Validators.minLength(15)]],
          tipo:[res.scorm.tipo,[Validators.required]]
        });
      });
    }
  }
  opciones: Option[] = [
    {value:'1',viewValue:'Obtener Palabras'},
    {value:'3',viewValue:'Palabras aleatorias'}
  ]
  miFormulario: FormGroup = this.fb.group({
    texto:['',[Validators.required, Validators.minLength(15)]],
    tipo:['1',[Validators.required]]
  });
  form2: FormGroup = this.fb.group({
    palabras: new FormArray([
      new FormControl('',[Validators.required])
    ])
  })
  
  crearRespuesta(){
    
    for(let p of this.pagesService.palabrasBorrar){
      this.pagesService.respuesta.push('');
    }
  } 
  guardarPalabras(){
    let texto = this.miFormulario.get('texto')!.value;
    texto = texto.replace(/,/g, "");      
      let resultado = texto.split(' ');
      this.palabrasObtenidas = resultado;
      this.pagesService.palabrasObtenidas = [...resultado];
      //this.pagesService.scorm.palabrasObtenidas = [...resultado]
      this.aleatorio();
  }

  leerLocal(nombre:string){
    const local = localStorage.getItem(nombre);
    
      if(local){
        const {tipo,borrar,obte} = JSON.parse(local);
        this.palabrasbor = borrar;
        this.palabrasObtenidas = obte;
        this. miFormulario = this.fb.group({
          texto:[obte.join(" "),[Validators.required, Validators.minLength(15)]],
          tipo:[tipo,[Validators.required]]
        });
    }else if(nombre === 'simple2'){
      this.leerBD();
    }
  }

  pasarDatos(){
    const tipo = this.miFormulario.get('tipo')!.value;
    this.scorm = {
            idUser:this.usuario.uid,
            palabrasObtenidas:this.palabrasObtenidas,
            palabrasBorrar:this.pagesService.palabrasBorrar,
            nombre:'',
            tipo:tipo
    }
    this.scormSimple = {
      tipo:tipo,
      obte:this.palabrasObtenidas,
      borrar:this.pagesService.palabrasBorrar,
      id:this.id
    }
          this.crearRespuesta();
          this.guardarLocal();
          this.pagesService.palabrasObtenidas = this.palabrasObtenidas;
          this.pagesService.scorm = this.scorm;
  }

  guardarLocal(){
    const id = this.ar.snapshot.params.id;
    if(id!=='0'){
      localStorage.setItem('simple2',JSON.stringify(this.scormSimple));
    }else{
      localStorage.removeItem('simple2');
      localStorage.setItem('simple1',JSON.stringify(this.scormSimple));
    }
  }

  borrarDatos(){
    // const id = this.ar.snapshot.params.id;
    // if(id!=='0'){
    //   this
    // }
    this.palabrasbor = [];
    this.pagesService.palabrasBorrar = [];
  

  }

  // guardarLocal(){
  //   const tipo = this.miFormulario.get('tipo')!.value;
  //   const id = this.ar.snapshot.params.id;
  //   if(id!=="0"){
      
  //     localStorage.setItem('borrar',JSON.stringify(this.pagesService.palabrasBorrar))
  //     localStorage.setItem('obtenidas',JSON.stringify(this.pagesService.scorm.palabrasObtenidas))
  //     this.scorm.tipo = tipo;
  //   }else{
  //     this.scorm = {
  //       idUser:this.usuario.uid,
  //       palabrasObtenidas:this.palabrasObtenidas,
  //       palabrasBorrar:[],
  //       nombre:'',
  //       tipo:tipo
  //     }
  //     localStorage.setItem('borrar',JSON.stringify(this.pagesService.palabrasBorrar))
  //     localStorage.setItem('obtenidas',JSON.stringify(this.pagesService.scorm.palabrasObtenidas))
  //   }
  //   this.pagesService.scorm = this.scorm;
  // }

 

  

  aleatorio(){
    this.aleatorias=[]
    for (let i = 0; i < this.palabrasObtenidas.length; i++) {
      let pal = this.palabrasObtenidas[Math.floor(Math.random()*this.palabrasObtenidas.length)];
      if (this.aleatorias.length<6 && pal.length>=5 && !this.aleatorias.includes(pal)){
        this.aleatorias.push(pal)
      }
    }
  }

  

  cambiar(){
    this.router.navigateByUrl('/main/prueba')
  }
  
  pintar(palabra:string):boolean{
    let res:string | undefined = '';
    const local = localStorage.getItem('simple');
    if(local){
      const borradas:string[] = JSON.parse(local).borrar;
      res = borradas.find(element=> element === palabra);
    }else{
      res = this.palabrasbor.find(element => element === palabra);
    }
    //this.pagesService.palabrasObtenidas = this.pal2;
    if(res){
      return true;
    }else{
      return false;
    }
  }
}
