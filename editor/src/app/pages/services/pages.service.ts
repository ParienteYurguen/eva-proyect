import { Injectable } from '@angular/core';
import { Scorm } from '../../interfaces/scorm.interface';

@Injectable({
  providedIn: 'root'
})
export class PagesService {


  palabrasObtenidas:string[] = [];
  palabrasBorrar:string[] = [];
  respuesta:string[] = [];
  temp:string[]=[];
  indices:number[]=[];

  scorm:Scorm = {
    idUser:'',
    tipo:'',
    palabrasBorrar:[],
    palabrasObtenidas:[],
    nombre:'',
    _id:''
  }
  constructor() { }
  eliminarBorrar(palabra:string){
    const index = this.buscarPalabra(palabra,this.palabrasBorrar);
    if(index!==-1){
      this.palabrasBorrar.splice(index,1);
    }
  }
  agregarRespuesta(palabra:string){
    let res = false;
    let i =0;
    while(res !== true){
      let espacio = this.respuesta[i];
      if(espacio === ""){
        res = true;
       this.respuesta[i] = palabra;
      }
      i++
    }
  }
  eliminarRespuesta(palabra:string){
    const index = this.buscarPalabra(palabra,this.respuesta);
    if(index!==-1){
      this.respuesta.splice(index,1,"");
    }
    console.log(this.respuesta)
  }
  agregarBorrar(palabra:string){
    let res = this.palabrasBorrar.find(element => element === palabra);
    if(!res){
      this.palabrasBorrar.push(palabra);
    }
  }
  buscarPalabra(palabra:string,arry:string[]):number{
    let res = -1;
    for(let i =0;i<arry.length;i++){
      if(arry[i] === palabra){
        res = i;
      }
    }
    return res;
  }
  // agregarPalabra(palabra:string, indice:number,bandera:boolean){
    
  //   const buscar = this.palabrasBorrar.find(m => m === palabra);
  //   if(!bandera){
  //     if(!buscar){
  //       this.palabrasBorrar.push(palabra);
  //     }else{
  //       this.borrarPalabra(palabra)
  //       this.recuperarPalabra(indice,palabra)
  //     }
  //   }else{
  //     this.palabrasBorrar.splice(indice,1,palabra)
  //   }
   
  // }
  // agregarRespuesta(palabra:string){
  //   this.respuesta.push(palabra);
  // }

  // borrarPalabra(palabra:string){
  //   if(this.palabrasBorrar.includes(palabra)){
  //     var indice = this.palabrasBorrar.indexOf(palabra); 
  //     this.palabrasBorrar.splice(indice, 1);
  //   }
  // }
  // recuperarPalabra(indice:number,palabra:string){
  //   this.palabrasObtenidas.splice(indice,1,palabra)
  // }

  // respuestaAñadida(palabra:string){
  //   const buscar = this.palabrasBorrar.find(m => m === palabra);
  //   if(!buscar){
  //     this.palabrasBorrar.push(palabra);
  //   }else{
  //     this.borrarRespuesta(palabra)
  //   }
  // }
  // borrarRespuesta(palabra:string){
  //   if(this.respuesta.includes(palabra)){
  //     var indice = this.respuesta.indexOf(palabra); 
  //     this.respuesta.splice(indice, 1);
  //   }
  // }
}
