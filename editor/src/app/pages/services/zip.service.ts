import { Injectable } from '@angular/core';
import * as JSZip from 'jszip';
import { saveAs } from 'file-saver';

@Injectable({
  providedIn: 'root'
})
export class ZipService {

    crearArray(arreglo:string[]):string{
        let res = '[';
        for(let i=0; i<arreglo.length;i++){
          if(i === arreglo.length-1){
            res += `"${arreglo[i]}"]`;
          }else{
            res += `"${arreglo[i]}",`
          }
        }
        return res;
      }

    descargarZip(palabras:string[],obtenidas:string[],respuestas:string[]){
      const arreglo = this.crearArray(palabras);
      const arreglo2 = this.crearArray(obtenidas);
      const arreglo3 = this.crearArray(respuestas)
      const zip = new JSZip();
      zip.file("logica.js", "var scorm = pipwerks.SCORM;"+`let respuestas = ${arreglo3} ;`+
      `const array = ${arreglo};`+
      "let i = 0;"+
              `function init(){
                scorm.version = "1.2";
                Mensaje("Iniciando el Curso.");
                cargarTexto()
                llenarPalabras();
                var callSucceeded = scorm.init();
                Mensaje("Curso iniciado correctamente? " + callSucceeded);
                scorm.set("cmi.core.credit",true);
                scorm.set("cmi.core.score.raw","0");
                ObtenerNombre()
              }
              function shuffle(array) {
                let temp=[];
                temp = array.sort(()=> Math.random() - 0.5);
                return temp
                }
            
              function buscarBoton(palabra){
                const arreglo = document.getElementsByClassName('boton')
                for(let i=0;i<=arreglo.length-1;i++){
                  let contenido = arreglo[i].textContent;
                  if(contenido===palabra){
                    arreglo[i].hidden = false;
                  }
                }
              }
              function frase(){
                let res = '';`+
                `const arreglo2 = ${arreglo2};`+
                `for(let pal of arreglo2){
                  res += pal + ' '; 
                }
                return res;
              }
              function ObtenerScore(corrctas,incorrectas){
                var score = scorm.get("cmi.core.score.raw");
                if(100 - score < 0.1){
                  score = 100;
                }
                document.getElementById('Puntaje').innerHTML =`+ 
                "`<p>${corrctas} de ${incorrectas}</p>"+
                 " <b> ${score} / 100  </b>"+
                 "<p>La frase es:</p>"+
                 "<p>${frase()}</p>`}"+
              `function AumentarScore(){
                let correctos=0;
                scorm.set("cmi.core.score.raw",0);
                let arrayRes = [];
                const nota = 100/array.length;
                for(let i = 0; i<=array.length;i++){
                  let pal1=array[i];
                  let pal2=respuestas[i];
                  console.log(array);
                  if(pal1===pal2){
                    arrayRes.push(i);
                    correctos++;
                    scorm.set("cmi.core.score.raw",Math.round(nota*correctos).toString());
                  }
                }
                //pintar(arrayRes);
                ObtenerScore(correctos,array.length);
              }
              
              function CompletarCurso(){
                  Mensaje("Marcando curso como Completado.");
                  var callSucceeded = scorm.set("cmi.core.lesson_status", "completed");
                  Mensaje("Curso Completado? " + callSucceeded);
              }`+
  
              `function pintar(array){
                for(let indice of array){`+
                  "let docu = document.getElementById(`${indice}`);"+
                 ` docu.className = "me-1 alert alert-success"
                }
              }`+
          
             "function eliminarRespuesta(indice){respuestas.splice(indice,1,'');const docu = document.getElementById(`${indice}`);if(docu){const pal2 = docu.textContent;docu.innerHTML = '';docu.className = 'me-1 alert alert-danger'; buscarBoton(pal2);}}"+
          
             `function cargarRespuesta(pal = '',indice){
               let bandera = false;
               let i = 0;
               while(i< respuestas.length && bandera===false){`+
                 "let docu = document.getElementById(`${i}`);"+
                 "let boton = document.getElementById(`pal${indice}`);"+
                ` if(respuestas[i]===''){
                   respuestas.splice(i,1,pal);
                   bandera=true;docu.innerHTML = pal;boton.hidden = true;}
                   i++;}
                  }\n`+
                   
               `function llenarPalabras(){
                const a = [...array];
                const mezcla = shuffle(a);
               for(let i=0;i<mezcla.length;i++){\n`+
                 "document.getElementById('contenedor').insertAdjacentHTML('beforeend',`<button class='btn btn-danger mt-2 boton me-1 text-center  col-12' id='pal${i}'"+`onclick="cargarRespuesta(`+"'${mezcla[i]}','${i}'"+`)"`+">${mezcla[i]}</button>`)"+
                `}}\n`+
          
              `function ObtenerNombre(){
                  var nombreUser = scorm.get("cmi.core.student_name");
                  document.getElementById('Nombre').innerHTML = '<b>' + nombreUser + '</b>';
              }
          `+
              `function end(){
                  Mensaje("Conexion terminada.");
                  var callSucceeded = scorm.quit();
                  Mensaje("Termino correctamente? " +callSucceeded);
              }
          
              function Mensaje(msg){
                  alert(msg);
              }`+
              
             ` function cargarTexto(){`+
                `let array = ${arreglo2};
                let borradas =${arreglo}
                let j = 0;
                for(let i=0; i<array.length;i++){
                  if(borradas.includes(array[i])){\n
                    if(i % 16 ===0 && i>0){
                      
                    `+
                  "document.getElementById('contenedorPalabras').insertAdjacentHTML('beforeend',`<span class='me-1 alert alert-danger'"+ `onclick="eliminarRespuesta(`+"'${j}'"+`)"`+ " id='${j}' ></span> <br><br>`);"+
                 `\n j++
                    }else{`+
                    "document.getElementById('contenedorPalabras').insertAdjacentHTML('beforeend',`<span class='me-1 alert alert-danger'"+ `onclick="eliminarRespuesta(`+"'${j}'"+`)"`+ " id='${j}' ></span>`);"+
                    `\n j++
                    };
                }else{\n
                  if(i % 16 ===0 && i>0){`+
                    " document.getElementById('contenedorPalabras').insertAdjacentHTML('beforeend',`<span class='me-1' >${array[i]}</span><br><br>`);"+
                 `}else{`+
                 " document.getElementById('contenedorPalabras').insertAdjacentHTML('beforeend',`<span class='me-1' >${array[i]}</span>`);"+
                 `}`+
               ` \n}}}`+
          
             ` window.onload = function (){
                  init();
              }
          
              window.onunload = function (){
                  end();
              }`);
      zip.file("estilos.css",`.col-12{
        width: 80%;
        margin: auto;
    }
    .row>.btn{
        margin-left: 40px;
    }   
    .border>.btn{
        margin-left: 15px;
    }
    span{
        cursor: pointer;
    }
    .alert{
      margin: 0px;
      padding: 7px;
      font-size: 13px;
  }
    `);
    zip.file('index.html',`<!DOCTYPE html>
    <html lang="en">
    <head>
      <meta charset="UTF-8">
      <title>SCORM</title>
      <script type="text/javascript" src="SCORM_API_wrapper.js"></script>
      <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-1BmE4kWBq78iYhFldvKuhfTAU6auU8tT94WrHftjDbrCEXSU1oBoqyl2QvZ6jIW3" crossorigin="anonymous">
      <link rel="stylesheet" href="estilos.css">
      <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/js/bootstrap.bundle.min.js" integrity="sha384-ka7Sk0Gln4gmtz2MlQnikT1wXgYsOg+OMhuP+IlRH9sENBO0LRn5q+8nbTov4+1p" crossorigin="anonymous"></script>
      <script type="text/javascript" src="logica.js">
      </script>
    </head>
    <body class="p-3">
      <h2>Realice la actividad </h2>
      <hr>
      <h4>Seleccione las palabras de los espacios vacios</h4>
      <h4 id="Nombre"></h4>
      <br><br>
      <div class="row">
        <div class="col-8">
          <div id="contenedorPalabras"></div>
        </div>
        <div class="col-4 border border-start border-secondary p-2">
          <button class="btn btn-primary col-11" onclick="AumentarScore()" type="button" data-bs-toggle="modal" data-bs-target="#staticBackdrop">Verificar respuestas</button>
          <div class="row" id="contenedor" align='center'>
    
          </div>
        
        </div>
      </div>
      
    
      <!-- Modal -->
    <div class="modal fade" id="staticBackdrop" data-bs-backdrop="static" data-bs-keyboard="false" tabindex="-1" aria-labelledby="staticBackdropLabel" aria-hidden="true">
      <div class="modal-dialog">
        <div class="modal-content">
        <div class="modal-header">
          <h5 class="modal-title" id="exampleModalLabel">Tu calificacion</h5>
        </div>
        <div class="modal-body" id="Puntaje">
          
        </div>
        <div class="modal-footer">
          <button class="btn btn-primary" onclick="CompletarCurso()" >Enviar calificacion</button>
        </div>
        </div>
      </div>
      </div>
    
    </body>
    </html>`);
    zip.file("imsmanifest.xml",`<?xml version="1.0" encoding="UTF-8"?>
    <manifest xmlns="http://www.imsproject.org/xsd/imscp_rootv1p1p2" xmlns:imsmd="http://www.imsglobal.org/xsd/imsmd_rootv1p2p1" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:adlcp="http://www.adlnet.org/xsd/adlcp_rootv1p2" identifier="pipwerksWrapperSCORM12" xsi:schemaLocation="http://www.imsproject.org/xsd/imscp_rootv1p1p2 imscp_rootv1p1p2.xsd http://www.imsglobal.org/xsd/imsmd_rootv1p2p1 imsmd_rootv1p2p1.xsd http://www.adlnet.org/xsd/adlcp_rootv1p2 adlcp_rootv1p2.xsd">
      <organizations default="pipwerks">
        <organization identifier="pipwerks" structure="hierarchical">
          <title>Cuestionarios</title>
          <item identifier="SCORM12_wrapper_test" isvisible="true" identifierref="pipfiles">
            <title>Cuestionarios</title>
          </item>
        </organization>
      </organizations>
      <resources>
        <resource identifier="pipfiles" type="webcontent" adlcp:scormtype="sco" href="index.html">
          <file href="index.html" />
          <file href="SCORM_API_wrapper.js" />
        </resource>
      </resources>
    </manifest>`);
    zip.file("SCORM_API_wrapper.js",`
    /* =====================================================================================
    
    SCORM wrapper v1.1.7 by Philip Hutchison, May 2008 (http://pipwerks.com).
    
    Copyright (c) 2008 Philip Hutchison
    MIT-style license. Full license text can be found at 
    http://www.opensource.org/licenses/mit-license.php
    
    This wrapper is designed to work with both SCORM 1.2 and SCORM 2004.
    
    Based on APIWrapper.js, created by the ADL and Concurrent Technologies
    Corporation, distributed by the ADL (http://www.adlnet.gov/scorm).
    
    SCORM.API.find() and SCORM.API.get() functions based on ADL code,
    modified by Mike Rustici (http://www.scorm.com/resources/apifinder/SCORMAPIFinder.htm),
    further modified by Philip Hutchison
    
    ======================================================================================== */
    
    
    var pipwerks = {};									//pipwerks 'namespace' helps ensure no conflicts with possible other "SCORM" variables
    pipwerks.UTILS = {};								//For holding UTILS functions
    pipwerks.debug = { isActive: true }; 				//Enable (true) or disable (false) for debug mode
    
    pipwerks.SCORM = {									//Define the SCORM object
        version:    null,              					//Store SCORM version.
      handleCompletionStatus: true,					//Whether or not the wrapper should automatically handle the initial completion status
      handleExitMode: true,							//Whether or not the wrapper should automatically handle the exit mode
        API:        { handle: null, 
              isFound: false },					//Create API child object
        connection: { isActive: false },				//Create connection child object
        data:       { completionStatus: null,
              exitStatus: null},				//Create data child object
        debug:      {}                 					//Create debug child object
    };
    
    
    
    /* --------------------------------------------------------------------------------
       pipwerks.SCORM.isAvailable
       A simple function to allow Flash ExternalInterface to confirm 
       presence of JS wrapper before attempting any LMS communication.
    
       Parameters: none
       Returns:    Boolean (true)
    ----------------------------------------------------------------------------------- */
    
    pipwerks.SCORM.isAvailable = function(){
      return true;     
    };
    
    
    
    // ------------------------------------------------------------------------- //
    // --- SCORM.API functions ------------------------------------------------- //
    // ------------------------------------------------------------------------- //
    
    
    /* -------------------------------------------------------------------------
       pipwerks.SCORM.API.find(window)
       Looks for an object named API in parent and opener windows
       
       Parameters: window (the browser window object).
       Returns:    Object if API is found, null if no API found
    ---------------------------------------------------------------------------- */
    
    pipwerks.SCORM.API.find = function(win){
    
        var API = null,
        findAttempts = 0,
            findAttemptLimit = 500,
        traceMsgPrefix = "SCORM.API.find",
        trace = pipwerks.UTILS.trace,
        scorm = pipwerks.SCORM;
    
        while ((!win.API && !win.API_1484_11) &&
               (win.parent) &&
               (win.parent != win) &&
               (findAttempts <= findAttemptLimit)){
    
                    findAttempts++; 
                    win = win.parent;
    
        }
    
      if(scorm.version){											//If SCORM version is specified by user, look for specific API
      
        switch(scorm.version){
          
          case "2004" : 
          
            if(win.API_1484_11){
          
              API = win.API_1484_11;
             
            } else {
              
              trace(traceMsgPrefix +": SCORM version 2004 was specified by user, but API_1484_11 cannot be found.");
              
            }
            
            break;
            
          case "1.2" : 
          
            if(win.API){
          
              API = win.API;
             
            } else {
              
              trace(traceMsgPrefix +": SCORM version 1.2 was specified by user, but API cannot be found.");
              
            }
            
            break;
          
        }
        
      } else {													//If SCORM version not specified by user, look for APIs
        
        if(win.API_1484_11) {									//SCORM 2004-specific API.
      
          scorm.version = "2004";								//Set version
          API = win.API_1484_11;
         
        } else if(win.API){										//SCORM 1.2-specific API
            
          scorm.version = "1.2";								//Set version
          API = win.API;
         
        }
    
      }
    
      if(API){
        
        trace(traceMsgPrefix +": API found. Version: " +scorm.version);
        trace("API: " +API);
    
      } else {
        
        trace(traceMsgPrefix +": Error finding API. Find attempts: " +findAttempts +". Find attempt limit: " +findAttemptLimit);
        
      }
      
        return API;
    
    };
    
    
    /* -------------------------------------------------------------------------
       pipwerks.SCORM.API.get()
       Looks for an object named API, first in the current window's frame
       hierarchy and then, if necessary, in the current window's opener window
       hierarchy (if there is an opener window).
    
       Parameters:  None. 
       Returns:     Object if API found, null if no API found
    ---------------------------------------------------------------------------- */
    
    pipwerks.SCORM.API.get = function(){
    
        var API = null,
        win = window,
        find = pipwerks.SCORM.API.find,
        trace = pipwerks.UTILS.trace; 
         
        if(win.parent && win.parent != win){ 
        
            API = find(win.parent); 
            
        } 
         
        if(!API && win.top.opener){ 
        
            API = find(win.top.opener); 
            
        } 
         
        if(API){  
        
            pipwerks.SCORM.API.isFound = true;
            
        } else {
        
            trace("API.get failed: Can't find the API!");
                                   
        }
         
        return API;
    
    };
              
    
    /* -------------------------------------------------------------------------
       pipwerks.SCORM.API.getHandle()
       Returns the handle to API object if it was previously set
    
       Parameters:  None.
       Returns:     Object (the pipwerks.SCORM.API.handle variable).
    ---------------------------------------------------------------------------- */
    
    pipwerks.SCORM.API.getHandle = function() {
      
      var API = pipwerks.SCORM.API;
         
        if(!API.handle && !API.isFound){
         
            API.handle = API.get();
         
        }
         
        return API.handle;
    
    };
         
    
    
    // ------------------------------------------------------------------------- //
    // --- pipwerks.SCORM.connection functions --------------------------------- //
    // ------------------------------------------------------------------------- //
    
    
    /* -------------------------------------------------------------------------
       pipwerks.SCORM.connection.initialize()
       Tells the LMS to initiate the communication session.
    
       Parameters:  None
       Returns:     Boolean
    ---------------------------------------------------------------------------- */
    
    pipwerks.SCORM.connection.initialize = function(){
                   
        var success = false,
        scorm = pipwerks.SCORM,
        completionStatus = pipwerks.SCORM.data.completionStatus,
        trace = pipwerks.UTILS.trace,
        makeBoolean = pipwerks.UTILS.StringToBoolean,
        debug = pipwerks.SCORM.debug,
        traceMsgPrefix = "SCORM.connection.initialize ";
    
        trace("connection.initialize called.");
    
        if(!scorm.connection.isActive){
    
            var API = scorm.API.getHandle(),
                errorCode = 0;
              
            if(API){
                   
          switch(scorm.version){
            case "1.2" : success = makeBoolean(API.LMSInitialize("")); break;
            case "2004": success = makeBoolean(API.Initialize("")); break;
          }
          
                if(success){
                
            //Double-check that connection is active and working before returning 'true' boolean
            errorCode = debug.getCode();
            
            if(errorCode !== null && errorCode === 0){
              
                      scorm.connection.isActive = true;
              
              if(scorm.handleCompletionStatus){
                
                //Automatically set new launches to incomplete 
                completionStatus = pipwerks.SCORM.status("get");
                
                if(completionStatus){
                
                  switch(completionStatus){
                    
                    //Both SCORM 1.2 and 2004
                    case "not attempted": pipwerks.SCORM.status("set", "incomplete"); break;
                    
                    //SCORM 2004 only
                    case "unknown" : pipwerks.SCORM.status("set", "incomplete"); break;
                    
                    //Additional options, presented here in case you'd like to use them
                    //case "completed"  : break;
                    //case "incomplete" : break;
                    //case "passed"     : break;	//SCORM 1.2 only
                    //case "failed"     : break;	//SCORM 1.2 only
                    //case "browsed"    : break;	//SCORM 1.2 only
                    
                  }
                  
                }
                
              }
            
            } else {
              
              success = false;
              trace(traceMsgPrefix +"failed. Error code: " +errorCode +" Error info: " +debug.getInfo(errorCode));
              
            }
                    
                } else {
            
            errorCode = debug.getCode();
                
            if(errorCode !== null && errorCode !== 0){
    
              trace(traceMsgPrefix +"failed. Error code: " +errorCode +" Error info: " +debug.getInfo(errorCode));
              
            } else {
              
              trace(traceMsgPrefix +"failed: No response from server.");
            
            }
                }
                  
            } else {
              
                trace(traceMsgPrefix +"failed: API is null.");
         
            }
              
        } else {
         
              trace(traceMsgPrefix +"aborted: Connection already active.");
              
         }
    
         return success;
    
    };
    
    
    /* -------------------------------------------------------------------------
       pipwerks.SCORM.connection.terminate()
       Tells the LMS to terminate the communication session
    
       Parameters:  None
       Returns:     Boolean
    ---------------------------------------------------------------------------- */
    
    pipwerks.SCORM.connection.terminate = function(){
         
        var success = false,
        scorm = pipwerks.SCORM,
        exitStatus = pipwerks.SCORM.data.exitStatus,
        completionStatus = pipwerks.SCORM.data.completionStatus,
        trace = pipwerks.UTILS.trace,
        makeBoolean = pipwerks.UTILS.StringToBoolean,
        debug = pipwerks.SCORM.debug,
        traceMsgPrefix = "SCORM.connection.terminate ";
    
    
        if(scorm.connection.isActive){
              
            var API = scorm.API.getHandle(),
                errorCode = 0;
                   
            if(API){
         
           if(scorm.handleExitMode && !exitStatus){
            
            if(completionStatus !== "completed" && completionStatus !== "passed"){
          
              switch(scorm.version){
                case "1.2" : success = scorm.set("cmi.core.exit", "suspend"); break;
                case "2004": success = scorm.set("cmi.exit", "suspend"); break;
              }
              
            } else {
              
              switch(scorm.version){
                case "1.2" : success = scorm.set("cmi.core.exit", "logout"); break;
                case "2004": success = scorm.set("cmi.exit", "normal"); break;
              }
              
            }
          
          }
       
          switch(scorm.version){
            case "1.2" : success = makeBoolean(API.LMSFinish("")); break;
            case "2004": success = makeBoolean(API.Terminate("")); break;
          }
                   
                if(success){
                        
                    scorm.connection.isActive = false;
                   
                } else {
                        
                    errorCode = debug.getCode();
                    trace(traceMsgPrefix +"failed. Error code: " +errorCode +" Error info: " +debug.getInfo(errorCode));
       
                }
                   
            } else {
              
                trace(traceMsgPrefix +"failed: API is null.");
         
            }
              
        } else {
         
            trace(traceMsgPrefix +"aborted: Connection already terminated.");
    
        }
    
        return success;
    
    };
    
    
    
    // ------------------------------------------------------------------------- //
    // --- pipwerks.SCORM.data functions --------------------------------------- //
    // ------------------------------------------------------------------------- //
    
    
    /* -------------------------------------------------------------------------
       pipwerks.SCORM.data.get(parameter)
       Requests information from the LMS.
    
       Parameter: parameter (string, name of the SCORM data model element)
       Returns:   string (the value of the specified data model element)
    ---------------------------------------------------------------------------- */
    
    pipwerks.SCORM.data.get = function(parameter){
    
        var value = null,
        scorm = pipwerks.SCORM,
        trace = pipwerks.UTILS.trace,
        debug = pipwerks.SCORM.debug,
        traceMsgPrefix = "SCORM.data.get(" +parameter +") ";
    
        if(scorm.connection.isActive){
    
            var API = scorm.API.getHandle(),
                errorCode = 0;
              
              if(API){
                   
          switch(scorm.version){
            case "1.2" : value = API.LMSGetValue(parameter); break;
            case "2004": value = API.GetValue(parameter); break;
          }
          
                errorCode = debug.getCode();
                   
                //GetValue returns an empty string on errors
                //Double-check errorCode to make sure empty string
                //is really an error and not field value
                if(value !== "" && errorCode === 0){
             
            switch(parameter){
              
              case "cmi.core.lesson_status": 
              case "cmi.completion_status" : scorm.data.completionStatus = value; break;
                        
              case "cmi.core.exit": 
              case "cmi.exit" 	: scorm.data.exitStatus = value; break;
              
            }
                   
                } else {
            
                    trace(traceMsgPrefix +"failed. Error code: " +errorCode +"Error info: " +debug.getInfo(errorCode));
                    
          }
              
            } else {
              
                trace(traceMsgPrefix +"failed: API is null.");
         
            }
              
        } else {
         
            trace(traceMsgPrefix +"failed: API connection is inactive.");
    
        }
      
      trace(traceMsgPrefix +" value: " +value);
      
        return String(value);
    
    };
              
              
    /* -------------------------------------------------------------------------
       pipwerks.SCORM.data.set()
       Tells the LMS to assign the value to the named data model element.
       Also stores the SCO's completion status in a variable named
       pipwerks.SCORM.data.completionStatus. This variable is checked whenever
       pipwerks.SCORM.connection.terminate() is invoked.
    
       Parameters: parameter (string). The data model element
                   value (string). The value for the data model element
       Returns:    Boolean
    ---------------------------------------------------------------------------- */
    
    pipwerks.SCORM.data.set = function(parameter, value){
    
        var success = false,
        scorm = pipwerks.SCORM,
        trace = pipwerks.UTILS.trace,
        makeBoolean = pipwerks.UTILS.StringToBoolean,
        debug = pipwerks.SCORM.debug,
        traceMsgPrefix = "SCORM.data.set(" +parameter +") ";
        
        
        if(scorm.connection.isActive){
              
            var API = scorm.API.getHandle(),
                errorCode = 0;
                   
            if(API){
                   
          switch(scorm.version){
            case "1.2" : success = makeBoolean(API.LMSSetValue(parameter, value)); break;
            case "2004": success = makeBoolean(API.SetValue(parameter, value)); break;
          }
          
                if(success){
            
            if(parameter === "cmi.core.lesson_status" || parameter === "cmi.completion_status"){
              
              scorm.data.completionStatus = value;
              
            }else if(parameter === "cmi.score"){
                        scorm.data.score = value;
                    }
            
          } else {
    
                    trace(traceMsgPrefix +"failed. Error code: " +errorCode +". Error info: " +debug.getInfo(errorCode));
    
                }
                   
            } else {
              
                trace(traceMsgPrefix +"failed: API is null.");
         
            }
              
        } else {
         
            trace(traceMsgPrefix +"failed: API connection is inactive.");
    
        }
         
        return success;
    
    };
              
    
    /* -------------------------------------------------------------------------
       pipwerks.SCORM.data.save()
       Instructs the LMS to persist all data to this point in the session
    
       Parameters: None
       Returns:    Boolean
    ---------------------------------------------------------------------------- */
    
    pipwerks.SCORM.data.save = function(){
    
        var success = false,
        scorm = pipwerks.SCORM,
        trace = pipwerks.UTILS.trace,
        makeBoolean = pipwerks.UTILS.StringToBoolean,
        traceMsgPrefix = "SCORM.data.save failed";
    
    
        if(scorm.connection.isActive){
    
            var API = scorm.API.getHandle();
              
            if(API){
              
          switch(scorm.version){
            case "1.2" : success = makeBoolean(API.LMSCommit("")); break;
            case "2004": success = makeBoolean(API.Commit("")); break;
          }
          
            } else {
              
                trace(traceMsgPrefix +": API is null.");
         
            }
              
        } else {
         
            trace(traceMsgPrefix +": API connection is inactive.");
    
        }
    
        return success;
    
    };
    
    
    pipwerks.SCORM.status = function (action, status){
      
        var success = false,
        scorm = pipwerks.SCORM,
        trace = pipwerks.UTILS.trace,
        traceMsgPrefix = "SCORM.getStatus failed",
        cmi = "";
    
      if(action !== null){
        
        switch(scorm.version){
          case "1.2" : cmi = "cmi.core.lesson_status"; break;
          case "2004": cmi = "cmi.completion_status"; break;
        }
        
        switch(action){
          
          case "get": success = pipwerks.SCORM.data.get(cmi); break;
          
          case "set": if(status !== null){
            
                  success = pipwerks.SCORM.data.set(cmi, status);
                  
                } else {
                  
                  success = false;
                  trace(traceMsgPrefix +": status was not specified.");
                  
                }
                
                break;
                
          default	  : success = false;
                trace(traceMsgPrefix +": no valid action was specified.");
                
        }
        
      } else {
        
        trace(traceMsgPrefix +": action was not specified.");
        
      }
      
      return success;
    
    };
    
    
    // ------------------------------------------------------------------------- //
    // --- pipwerks.SCORM.debug functions -------------------------------------- //
    // ------------------------------------------------------------------------- //
    
    
    /* -------------------------------------------------------------------------
       pipwerks.SCORM.debug.getCode
       Requests the error code for the current error state from the LMS
    
       Parameters: None
       Returns:    Integer (the last error code).
    ---------------------------------------------------------------------------- */
    
    pipwerks.SCORM.debug.getCode = function(){
         
        var API = pipwerks.SCORM.API.getHandle(),
        scorm = pipwerks.SCORM,
        trace = pipwerks.UTILS.trace,
            code = 0;
    
        if(API){
    
        switch(scorm.version){
          case "1.2" : code = parseInt(API.LMSGetLastError(), 10); break;
          case "2004": code = parseInt(API.GetLastError(), 10); break;
        }
             
        } else {
         
            trace("SCORM.debug.getCode failed: API is null.");
    
        }
         
        return code;
        
    };
    
    
    /* -------------------------------------------------------------------------
       pipwerks.SCORM.debug.getInfo()
       "Used by a SCO to request the textual description for the error code
       specified by the value of [errorCode]."
    
       Parameters: errorCode (integer).  
       Returns:    String.
    ----------------------------------------------------------------------------- */
    
    pipwerks.SCORM.debug.getInfo = function(errorCode){
         
        var API = pipwerks.SCORM.API.getHandle(),
        scorm = pipwerks.SCORM,
        trace = pipwerks.UTILS.trace,
            result = "";
         
        
        if(API){
              
        switch(scorm.version){
          case "1.2" : result = API.LMSGetErrorString(errorCode.toString()); break;
          case "2004": result = API.GetErrorString(errorCode.toString()); break;
        }
        
        } else {
         
            trace("SCORM.debug.getInfo failed: API is null.");
    
        }
         
        return String(result);
    
    };
    
    
    /* -------------------------------------------------------------------------
       pipwerks.SCORM.debug.getDiagnosticInfo
       "Exists for LMS specific use. It allows the LMS to define additional
       diagnostic information through the API Instance."
    
       Parameters: errorCode (integer).  
       Returns:    String (Additional diagnostic information about the given error code).
    ---------------------------------------------------------------------------- */
    
    pipwerks.SCORM.debug.getDiagnosticInfo = function(errorCode){
    
        var API = pipwerks.SCORM.API.getHandle(),
        scorm = pipwerks.SCORM,
        trace = pipwerks.UTILS.trace,
            result = "";
        
        if(API){
    
        switch(scorm.version){
          case "1.2" : result = API.LMSGetDiagnostic(errorCode); break;
          case "2004": result = API.GetDiagnostic(errorCode); break;
        }
        
        } else {
         
            trace("SCORM.debug.getDiagnosticInfo failed: API is null.");
    
        }
    
        return String(result);
    
    };
    
    
    // ------------------------------------------------------------------------- //
    // --- Shortcuts! ---------------------------------------------------------- //
    // ------------------------------------------------------------------------- //
    
    // Because nobody likes typing verbose code.
    
    pipwerks.SCORM.init = pipwerks.SCORM.connection.initialize;
    pipwerks.SCORM.get  = pipwerks.SCORM.data.get;
    pipwerks.SCORM.set  = pipwerks.SCORM.data.set;
    pipwerks.SCORM.save = pipwerks.SCORM.data.save;
    pipwerks.SCORM.quit = pipwerks.SCORM.connection.terminate;
    
    
    
    // ------------------------------------------------------------------------- //
    // --- pipwerks.UTILS functions -------------------------------------------- //
    // ------------------------------------------------------------------------- //
    
    
    /* -------------------------------------------------------------------------
       pipwerks.UTILS.StringToBoolean()
       Converts 'boolean strings' into actual valid booleans.
       
       (Most values returned from the API are the strings "true" and "false".)
    
       Parameters: String
       Returns:    Boolean
    ---------------------------------------------------------------------------- */
    
    pipwerks.UTILS.StringToBoolean = function(string){
         switch(string.toLowerCase()) {
              case "true": case "yes": case "1": return true;
              case "false": case "no": case "0": case null: return false; 
              default: return Boolean(string);
         }     
    };
    
    
    
    /* -------------------------------------------------------------------------
       pipwerks.UTILS.trace()
       Displays error messages when in debug mode.
    
       Parameters: msg (string)  
       Return:     None
    ---------------------------------------------------------------------------- */
    
    pipwerks.UTILS.trace = function(msg){
    
         if(pipwerks.debug.isActive){
         
        //Firefox users can use the 'Firebug' extension's console.
        if(window.console && window.console.firebug){
          console.log(msg);
        } else {
          //alert(msg);
        }
        
         }
    };`)
      zip.generateAsync({type:"blob"})
      .then(function(content) {
      // see FileSaver.js
      saveAs(content, "example.zip");
  });

      }
}