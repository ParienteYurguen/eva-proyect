import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class FileService {

  constructor(private http:HttpClient) { }

  descargarArchivo(){
    const ruta = "http://localhost:8080/api/";
    return this.http.get(`${ruta}user`);
  }
}
