import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Respuesta2, Scorm } from 'src/app/interfaces/scorm.interface';
import { Respuesta } from '../../interfaces/scorm.interface';

@Injectable({
  providedIn: 'root'
})
export class ScormsService {
  apiPath:string = 'http://localhost:8080/api/';
  constructor(private http:HttpClient) { }

  guardarScorm(idUser:string,palabrasBorrar:string[],palabrasObtenidas:string[],nombre:string,tipo:string){
    const body = {idUser,palabrasBorrar,palabrasObtenidas,nombre,tipo}
    return this.http.post(`${this.apiPath}scorm`,body);
  }
  cargarScorm(idUser:string){
    return this.http.get<Respuesta>(`${this.apiPath}scorm/user/${idUser}`);
  }
  editarScorm(id:string,scorm:Scorm){
    return this.http.put(`${this.apiPath}scorm/${id}`,scorm);
  }
  eliminarScorm(id:string){
    return this.http.delete(`${this.apiPath}scorm/${id}`)
  }
  getPorId(id:string){
    return this.http.get<Respuesta2>(`${this.apiPath}scorm/${id}`)
  }

}
