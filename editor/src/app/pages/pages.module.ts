import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { PagesRoutingModule } from './pages-routing.module';
import { EditorComponent } from './editor/editor.component';
import { MaterialModule } from '../material/material.module';
import { ReactiveFormsModule } from '@angular/forms';
import { PalabraComponent } from '../components/palabra/palabra.component';
import { PruebaComponent } from './prueba/prueba.component';
import { MenuComponent } from '../components/menu/menu.component';
import { ListaComponent } from './lista/lista.component';
import { TipoPipe } from './pipes/tipo.pipe';


@NgModule({
  declarations: [
    EditorComponent,
    PalabraComponent,
    PruebaComponent,
    MenuComponent,
    ListaComponent,
    TipoPipe
  ],
  imports: [
    CommonModule,
    PagesRoutingModule,
    MaterialModule,
    ReactiveFormsModule,
    
  ]
})
export class PagesModule { }
