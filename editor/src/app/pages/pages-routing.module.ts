import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { EditorComponent } from './editor/editor.component';
import { PruebaComponent } from './prueba/prueba.component';
import { ListaComponent } from './lista/lista.component';
import { AuthGuard } from '../guards/auth.guard';
import { PruebaGuard } from '../guards/prueba.guard';

const routes: Routes = [
  {
    path:'',
    children:[
      {path:'editor/:id',component:EditorComponent},
      {path:'prueba',component:PruebaComponent,canActivate:[PruebaGuard],canLoad:[PruebaGuard]},
      {path:'scorms',component:ListaComponent}
    ],
    canLoad:[AuthGuard],
    canActivate:[AuthGuard]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class PagesRoutingModule { }
