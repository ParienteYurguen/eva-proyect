import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'tipo'
})
export class TipoPipe implements PipeTransform {

  transform(value:string): unknown {
    if(value === "1"){
      return "Obtener palabras"
    }else{
      return "Palabras aleatorias"
    }
  }

}
