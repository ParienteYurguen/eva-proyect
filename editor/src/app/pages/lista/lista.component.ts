import { Component, OnInit } from '@angular/core';
import { Scorm } from '../../interfaces/scorm.interface';
import { ScormsService } from '../services/scorms.service';
import { ZipService } from '../services/zip.service';



@Component({
  selector: 'app-lista',
  templateUrl: './lista.component.html',
  styleUrls: ['./lista.component.css']
})
export class ListaComponent implements OnInit {
  

  dataSource:Scorm[]=[];
  pagesService: any;
  constructor(private scormService:ScormsService, private zipService:ZipService) { }
  usuario = JSON.parse(localStorage.getItem('uid')!).uid;
  
  ngOnInit(): void {
   this.cargarDatos();
   
  }
  
  cargarDatos(){
    this.scormService.cargarScorm(this.usuario)
    .subscribe(res => this.dataSource = res.scorms);
   
  }
  eliminar(id:string){
    this.scormService.eliminarScorm(id)
    .subscribe(res => this.cargarDatos())
  }
  crearLink(id:string):string{
    if(localStorage.getItem('simple2')){
      localStorage.removeItem('simple2');

    }
    return `/main/editor/${id}`
  }
  crearRespuesta(borrador:string[]){
    let res = [];
    for(let p of borrador){
      res.push('');
    }
    return res;
  } 
  descargarScorm(palabras:string[],obtenidas:string[]){
    const res = this.crearRespuesta(palabras);
    this.zipService.descargarZip(palabras,obtenidas,res)
  }
}
