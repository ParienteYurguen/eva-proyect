var scorm = pipwerks.SCORM;
let respuestas = ["","",""];
const array = ["tal","estas","bien"];
//let mezclado = shuffle(array);
let i = 0;

	function init(){
		scorm.version = "1.2";
		Mensaje("Iniciando el Curso.");
		cargarTexto()
		llenarPalabras();
		var callSucceeded = scorm.init();
		Mensaje("Curso iniciado correctamente? " + callSucceeded);
		scorm.set("cmi.core.credit",true);
		scorm.set("cmi.core.score.raw","0");
		ObtenerNombre()
	}
	function shuffle(array) {
		let temp=[];
		temp = array.sort(()=> Math.random() - 0.5);
		return temp
	  }

	function ObtenerScore(corrctas,incorrectas){
		var score = scorm.get("cmi.core.score.raw");
		document.getElementById('Puntaje').innerHTML = 
		`<p>${corrctas} de ${incorrectas}</p>
		<b> ${score} / 100  </b>`;
	}

	function AumentarScore(){
		let correctos =0;
		scorm.set("cmi.core.score.raw",0);
		let arrayRes = [];
		for(let i = 0; i<array.length;i++){
			let pal1=array[i];
			let pal2=respuestas[i];
			if(pal1===pal2){
				arrayRes.push(i);
				scorm.set("cmi.core.score.raw",(Number(scorm.get("cmi.core.score.raw"))+25).toString());
				correctos++;
			}
		}
		pintar(arrayRes);
		ObtenerScore(correctos,array.length);
		
	}
	function pintar(array){
		for(let indice of array){
			let docu = document.getElementById(`${indice}`);
			docu.className = "me-1 alert alert-success"
		}
	}
	function CompletarCurso(){
		Mensaje("Marcando curso como Completado.");
		var callSucceeded = scorm.set("cmi.core.lesson_status", "completed");
		Mensaje("Curso Completado: " + callSucceeded);
	}
	function buscarBoton(palabra){
		const arreglo = document.getElementsByClassName('boton')
		for(let i=0;i<=arreglo.length-1;i++){
			let contenido = arreglo[i].textContent;
			if(contenido===palabra){
				arreglo[i].hidden = false;
			}
		}
	}

	function eliminarRespuesta(indice){
		respuestas.splice(indice,1,"");
		const docu = document.getElementById(`${indice}`);
		console.log(indice,docu.textContent);
		if(docu){
			const pal2 = docu.textContent;
			docu.innerHTML = "";
			docu.className = "me-1 alert alert-danger" 
			buscarBoton(pal2);
		}
		
	}

	function cargarRespuesta(pal = "",indice){
		let bandera = false;
		let i = 0;
		while(i< respuestas.length && bandera===false){
			let docu = document.getElementById(`${i}`);
			let boton = document.getElementById(`pal${indice}`);
			if(respuestas[i]===""){
				respuestas.splice(i,1,pal);
				bandera=true;
				docu.innerHTML = pal;
				boton.hidden = true;
			}
			i++;
		}
		//Eliminar();
	}
	function llenarPalabras(){
		const a = [...array];
		const mezcla = shuffle(a);
		for(let i=0;i<mezcla.length;i++){
			document.getElementById("contenedor").insertAdjacentHTML("beforeend",`
			<button class="btn btn-danger mt-2 me-1 boton text-center  col-12" id="pal${i}" onclick="cargarRespuesta('${mezcla[i]}','${i}')">${mezcla[i]}</button>
		`)
		
	}
	}


	function ObtenerNombre(){
		var nombreUser = scorm.get("cmi.core.student_name");
		document.getElementById('Nombre').innerHTML = '<b>' + nombreUser + '</b>';
	}

	function end(){
		Mensaje("Conexion terminada.");
		var callSucceeded = scorm.quit();
		Mensaje("Termino correctamente? " +callSucceeded);
	}

	function Mensaje(msg){
		alert(msg);
	}
	
	function cargarTexto(){
		let obtenidas = ["Hola", "que","tal",  "como", "estas", "yo", "muy", "bien","Hola", "que","tal",  "como", "estas", "yo", "muy", "bien" ,"Hola", "que","tal",  "como", "estas", "yo", "muy", "bien"  ]
		let array = ["tal","estas","bien"];
		let j = 0;
		for(let i=0; i<obtenidas.length;i++){
			if(array.includes(obtenidas[i])){
				if(i % 16 ===0 && i>0){

					document.getElementById("contenedorPalabras").insertAdjacentHTML("beforeend",
					`<span class="me-1 alert alert-danger" onclick="eliminarRespuesta('${j}')" id="${j}" ></span> <br>`
					);
					j++;
				}else{
					document.getElementById("contenedorPalabras").insertAdjacentHTML("beforeend",
					`<span class="me-1 alert alert-danger" onclick="eliminarRespuesta('${j}')" id="${j}" ></span>`
					);
					j++;
				}
			}else{
				if(i%16 === 0 && i>0){

					document.getElementById("contenedorPalabras").insertAdjacentHTML("beforeend",
					`<aside class="me-1" >${obtenidas[i]}</aside> <br>`
					);
				}else{
					document.getElementById("contenedorPalabras").insertAdjacentHTML("beforeend",
					`<aside class="me-1" >${obtenidas[i]}</aside>`
					);
				}
			}
		}

	}

	window.onload = function (){
		init();
	}

	window.onunload = function (){
		end();
	}