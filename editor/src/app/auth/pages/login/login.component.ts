import { Component, OnInit } from '@angular/core';
import { AuthService } from '../../services/auth.service';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import Swal from "sweetalert2";
import { Router } from '@angular/router';
@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {

  constructor(private authService:AuthService, private fb:FormBuilder,
              private router:Router) { }
  miFormulario: FormGroup = this.fb.group({
    correo:['', [Validators.required, Validators.email]],
    password:['', [Validators.required, Validators.minLength(6)]]
  })
  ngOnInit(): void {
  }

  login(){
    const {correo,password} = this.miFormulario.value;
    this.authService.login(correo,password)
    .subscribe(res => {
      if(res === true){
        this.router.navigateByUrl('/main/editor/0')
      }else{
        Swal.fire('Error',res,'error')
      }
    })
  }

}
