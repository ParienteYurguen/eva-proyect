import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { AuthService } from '../../services/auth.service';
import Swal from "sweetalert2";

@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.css']
})
export class RegisterComponent implements OnInit {

  constructor(private authService:AuthService, private fb:FormBuilder,
    private router:Router) { }
  
    miFormulario: FormGroup = this.fb.group({
      correo:['', [Validators.required, Validators.email]],
      password:['', [Validators.required, Validators.minLength(6)]],
      nombre:['', [Validators.required, Validators.pattern]]
    });
    
  ngOnInit(): void {
  }

  registrar(){
    const {nombre,correo,password} = this.miFormulario.value;
    this.authService.register(nombre,correo,password)
    .subscribe(res => {
      if(res === true){
        this.router.navigateByUrl('/main/editor/0');
      }else{
        
        Swal.fire('Error',res,'error')
      }
    })
  }

}
