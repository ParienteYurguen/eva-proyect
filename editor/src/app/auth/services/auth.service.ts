import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { AuthResponse, Usuario } from '../../interfaces/auth.interface';
import { catchError, map, tap } from "rxjs/operators";
import { of} from 'rxjs';
@Injectable({
  providedIn: 'root'
})
export class AuthService {
  apiPath:string = 'http://localhost:8080/api/';
  private _usuario!: Usuario; 

  get usuario(){
    return {...this._usuario}
  }
  constructor(private http:HttpClient) { }

  login(correo:string, password:string){
    const body = {correo,password};
    return this.http.post<AuthResponse>(`${this.apiPath}auth/login`,body)
    .pipe(
      tap(res => {
        if(res.ok){
          localStorage.setItem('token',res.token!)
          this._usuario=res.usuario
          localStorage.setItem('uid',JSON.stringify(res.usuario))
        }
      }),
      map(res => res.ok),
      catchError(err => of(err.error.msg))
    );
  }

  register(nombre:string,correo:string,password:string){
    const body = {nombre,correo,password};
    return this.http.post<AuthResponse>(`${this.apiPath}usuarios`,body)
    .pipe(
      tap(res =>{
        if(res.ok){
          localStorage.setItem('token',res.token!)
          this._usuario=res.usuario;
          localStorage.setItem('uid',JSON.stringify(res.usuario))
        }
      }),
      map(res => res.ok),
      catchError(err => of(err.error.errors[0].msg))
    )
  }
}
